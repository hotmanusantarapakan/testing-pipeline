package main.jenkins.training

class CallResource implements Serializable{
    private final def script;
    private String config
    CallResource(def script){
        this.script = script
    }

    def setResource(){
        this.config = this.script.libraryResource 'id/jenkins/legacy/config.json'
        this.script.println(this.config)
        return this
    }

    def parsingJson(){
        Object pars = this.script.readJSON text: this.config
        this.script.println(pars)
        this.script.println(pars.server)
    }

    def callStringResource(){
        String aja = this.script.libraryResource 'id/jenkins/legacy/ini_hacker'
        this.script.println(aja)
    }
}