package main.jenkins.training

class SegiEmpatBedaSisi extends AbstractSegiEmpat implements BangunDatar, Serializable {
    private def script
    SegiEmpatBedaSisi(def script){
        this.script = script
    }
    
    def pasangPanjang(){
        this.panjang = 10
        this.script.println('set panjang :' + this.panjang)
        return this
    }

    def pasangLebar(int lebar){
        this.lebar = lebar
        this.script.node{
            this.script.sh("echo ${this.lebar}")
        }
        return this
    }

    int keliling(){
        return this.panjang * 2 + this.lebar * 2
    }   
    int luasBangun(int panjang, int lebar){
        return panjang * lebar
    }
}